use anyhow::Result;
use clap::Parser;
use data_encoding::BASE32HEX_NOPAD;
use image::io::Reader as ImageReader;
use mozjpeg;
use ring::digest;
use std::fs;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use tokio;
use tokio::sync::Semaphore;
use tokio::task;
use walkdir::WalkDir;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Source of the image file. Image files are recursively searched.
    #[clap(short)]
    source: PathBuf,

    /// Destination to write the resized images to
    #[clap(short)]
    destination: PathBuf,

    /// Width of the photo stand
    #[clap(long, default_value_t = 800)]
    width: u32,

    /// Height of the photo stand
    #[clap(long, default_value_t = 480)]
    height: u32,

    /// Concurrency
    #[clap(short, default_value_t = 8)]
    jobs: usize,

    /// Output JPEG Quality
    #[clap(short, long, default_value_t = 90.0)]
    quality: f32,

    /// Set output file name to digest of input path; useful for quasi-randomizing slideshow order
    #[clap(long, action)]
    digest: bool,
}

async fn resize_image(
    src_path: PathBuf,
    destination_path: PathBuf,
    width: u32,
    height: u32,
    quality: f32,
) -> Result<()> {
    let img = ImageReader::open(&src_path)?.decode()?;
    let small_img = if img.width() > width || img.height() > height {
        img.resize(width, height, image::imageops::FilterType::Lanczos3)
            .to_rgb8()
    } else {
        img.to_rgb8()
    };

    println!(
        "{} -> {}: {}x{}",
        src_path.display(),
        destination_path.display(),
        small_img.width(),
        small_img.height()
    );
    if let Some(dst_dir) = destination_path.parent() {
        let _ = fs::create_dir_all(dst_dir);
    }
    let dst_file = File::create(destination_path)?;
    let mut comp = mozjpeg::Compress::new(mozjpeg::ColorSpace::JCS_RGB);
    comp.set_fastest_defaults(); // generate baseline jpeg instead of progressive
    comp.set_quality(quality);
    comp.set_size(
        small_img.width().try_into()?,
        small_img.height().try_into()?,
    );
    let mut comp = comp.start_compress(dst_file)?;
    comp.write_scanlines(small_img.as_raw())?;
    let _ = comp.finish()?;

    Ok(())
}

fn destination_path_for_source(src_dir: &Path, src: &Path, digest_path: bool) -> PathBuf {
    let dst_path_clear = if src.is_absolute() {
        let src_without_prefix = src.strip_prefix(src_dir);
        if src_without_prefix.is_ok() {
            src_without_prefix.unwrap().to_path_buf()
        } else {
            PathBuf::from(src)
        }
    } else {
        PathBuf::from(src)
    };
    if digest_path {
        let hash = digest::digest(
            &digest::SHA256,
            dst_path_clear.as_os_str().as_encoded_bytes(),
        );
        let b32 = BASE32HEX_NOPAD.encode(hash.as_ref());
        let mut hashed_path = PathBuf::from(&b32[0..8]);
        hashed_path.set_extension("jpg");
        hashed_path
    } else {
        dst_path_clear
    }
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    let sem = Arc::new(Semaphore::new(args.jobs));
    let mut tasks = Vec::with_capacity(1024);

    for entry in WalkDir::new(&args.source)
        .into_iter()
        .filter_map(Result::ok)
        .filter(|e| {
            let filetype = e.file_type();
            let path = e.path();
            if filetype.is_file()
                || (filetype.is_symlink()
                    && fs::symlink_metadata(path).unwrap().file_type().is_file())
            {
                let ext_opt = path.extension();
                if ext_opt.is_some() {
                    let ext = ext_opt.unwrap();
                    ext.eq_ignore_ascii_case("jpg") || ext.eq_ignore_ascii_case("jpeg")
                } else {
                    false
                }
            } else {
                false
            }
        })
    {
        let permit = Arc::clone(&sem).acquire_owned().await;
        let path_p = PathBuf::from(entry.path());
        let dst_path_base =
            destination_path_for_source(args.source.as_ref(), path_p.as_ref(), args.digest);
        let path_dst_p = args.destination.join(dst_path_base);

        let join_handle = task::spawn(async move {
            let _permit = permit;
            let path = path_p;
            let dst_path = path_dst_p;

            resize_image(path, dst_path, args.width, args.height, args.quality).await
        });
        tasks.push(join_handle);
    }
    for join_handle in tasks {
        let _ = join_handle.await.unwrap();
    }
}
