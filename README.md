# デジタルフォトフレーム用に画像を縮小する

デジタルフォトフレームはしばしばギフトでもらったりするが、データを書き込んでおくのが煩雑で使わないままになっていたりしませんか。
このプログラムでは、写真が保存されているディレクトリを指定することで、その中にあるJPEG画像をフォトフレーム用に一括でリサイズして保存してくれます。

並列動作に対応しているので、マルチコアCPU を活用できます。

## 典型的な使い方

```sh
cd ~/Pictures
photostand -s 2024/Jan/ -d /mnt/sdcard --width 800 --height 480
```

→ディレクトリ ~/Pictures/2024/Jan/ 以下にある画像ファイルを縮小して、 /mnt/sdcard/2024/Jan/dsc0001.jpg などとして書き込みます。
画面解像度800x480でうまく映せるよう、アスペクト比を保持して縮小します。

### 便利機能

`--digest` を指定すると宛先ファイルを8.3形式でダイジェストから命名します。
ランダム表示ができない機種でも大体
ランダムになるので便利。

## 説明

画像の圧縮にはmozjpeg を使っているんですが、デジタルフォトフレームの中にはProgressive JPEGに対応していないものもあるため、パラメータをlibjpeg-turbo相当で使っています。
Mozjpeg を使う意味がない。

## 利用可能なオプション

```
Usage: photostand [OPTIONS] -s <SOURCE> -d <DESTINATION>

Options:
  -s <SOURCE>              Source of the image file. Image files are recursively searched
  -d <DESTINATION>         Destination to write the resized images to
      --width <WIDTH>      Width of the photo stand [default: 800]
      --height <HEIGHT>    Height of the photo stand [default: 480]
  -j <JOBS>                Concurrency [default: 8]
  -q, --quality <QUALITY>  Output JPEG Quality [default: 90]
      --digest             Set output file name to digest of input path; useful for quasi-randomizing slideshow order
  -h, --help               Print help
  -V, --version            Print version
```

## ライセンス

Apache-2.0
